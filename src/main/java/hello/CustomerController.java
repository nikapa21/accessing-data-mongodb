package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository repository;

    @RequestMapping("/customers")
    public List<Customer> getAllCustomers() {

        List<Customer> customers = repository.findAll();
        return customers;

    }
    @RequestMapping("/customersWithFirstName")
    public List<Customer> getCustomerFirstName(@RequestParam(value="firstName", required=false) String firstName){
        List<Customer> customers = repository.findByFirstName(firstName);
        return customers;
    }

    @RequestMapping("/insertCustomer")
    public void insertCustomer(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName){
        repository.save(new Customer(firstName, lastName));
        System.out.println("New Customer added!");
    }
    @RequestMapping("/deleteAll")
    public void deleteAllCustomers(){
        repository.deleteAll();
    }

    @RequestMapping("/deleteByFirstName")
    public void deleteByFirstName(@RequestParam(value="firstName") String firstName){
        List<Customer> customers = repository.findByFirstName(firstName);
        repository.delete(customers);
    }

}